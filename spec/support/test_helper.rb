module TestHelper
  def is_logged_in?
    !session[:user_id].nil?
  end

  def log_in_as(user, remember_me: '1')
    post login_path, params: { session: { email: user.email, password: user.password, remember_me: remember_me } }
  end

  def valid_login(user)
    visit root_path
    click_link 'Log in'
    fill_in 'メールアドレス', with: user.email
    fill_in 'パスワード', with: user.password
    click_button 'Log in'
  end
end
