FactoryBot.define do
  factory :micropost do
    association :user
    title { 'micropost test' }
    content { 'micropost test' }
    created_at { 10.minutes.ago }
    picture { nil }

    trait :yesterday do
      title { 'yesterday' }
      content { 'yesterday' }
      created_at { 1.day.ago }
      picture { nil }
    end

    trait :day_before_yesterday do
      title { 'day_before_yesterday' }
      content { 'day_before_yesterday' }
      created_at { 2.days.ago }
      picture { nil }
    end

    trait :now do
      title { 'now!' }
      content { 'now!' }
      created_at { Time.zone.now }
      picture { nil }
    end
  end
end
