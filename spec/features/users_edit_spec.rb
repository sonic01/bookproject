require 'rails_helper'

RSpec.feature "Edit", type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before do
    valid_login(user)
    click_on 'Settings'
  end

  it 'ユーザー情報の編集に失敗する' do
    fill_in '名前', with: ' '
    fill_in 'メールアドレス', with: 'user@invalid'
    fill_in 'パスワード', with: 'foo'
    fill_in 'パスワード確認', with: 'bar'
    click_on '変更を保存'
    aggregate_failures do
      expect(current_path).to eq user_path(user)
      expect(has_css?('.alert-danger')).to be_truthy
    end
  end

  it 'ユーザー情報の編集に成功する' do
    fill_in '名前', with: 'Foo Bar'
    fill_in 'メールアドレス', with: 'foo@bar.com'
    fill_in 'パスワード', with: ''
    fill_in 'パスワード確認', with: ''
    click_on '変更を保存'
    aggregate_failures do
      expect(current_path).to eq root_path
      expect(has_css?('.alert-success')).to be_truthy
    end
  end
end
