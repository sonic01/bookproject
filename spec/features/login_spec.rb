require 'rails_helper'

RSpec.feature 'Login', type: :feature do
  before do
    visit login_path
  end

  describe '有効な値が入力された時ログインが成功する' do
    subject { page }

    let!(:user) { create(:user, email: 'loginuser@example.com', password: 'password') }

    before do
      fill_in 'メールアドレス', with: 'loginuser@example.com'
      fill_in 'パスワード', with: 'password'
      click_button 'Log in'
    end

    it 'ログインした時のページのレイアウトの確認' do
      is_expected.to have_current_path user_path(user)
      is_expected.not_to have_link nil, href: login_path
      is_expected.to have_link 'Profile', href: user_path(user)
      is_expected.to have_link 'Settings', href: edit_user_path(user)
      is_expected.to have_link 'Log out', href: logout_path
      is_expected.to have_link '+', href: new_micropost_path
    end

    it 'ログアウトした時のページのレイアウトの確認' do
      click_link 'Log out'
      is_expected.to have_current_path root_path
      is_expected.to have_link 'Sign up', href: signup_path
      is_expected.to have_link 'Log in', href: login_path
      is_expected.not_to have_link nil, href: logout_path
      is_expected.not_to have_link nil, href: user_path(user)
      is_expected.not_to have_link nil, href: new_micropost_path
    end
  end

  describe '無効な値が入力された時ログインが失敗する' do
    subject { page }

    before do
      fill_in 'メールアドレス', with: ''
      fill_in 'パスワード', with: ''
      click_button 'Log in'
    end

    it 'フラッシュメッセージが表示される' do
      is_expected.to have_selector('.alert-danger', text: 'メールとパスワードの組み合わせが無効です')
      is_expected.to have_current_path login_path
    end

    context '違うページにアクセスした時' do
      before { visit root_path }

      it 'フラッシュメッセージが消える' do
        is_expected.not_to have_selector('.alert-danger', text: 'メールとパスワードの組み合わせが無効です')
      end
    end
  end
end
