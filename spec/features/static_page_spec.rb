require 'rails_helper'

RSpec.feature 'StaticPages', type: :feature do
  describe 'Home page' do
    before do
      visit root_path
    end

    it 'タイトルが正しく表示されていること' do
      expect(page).to have_title full_title('')
    end

    it "'今すぐ登録'のボタンを押すと登録ページに移行する" do
      click_on '今すぐユーザー登録を行う'
      visit signup_path
      expect(page).to have_content 'Sign up'
    end
  end
end
