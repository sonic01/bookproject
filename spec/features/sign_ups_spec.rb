require 'rails_helper'

RSpec.feature 'SignUps', type: :feature do
  describe 'user create a new account' do
    context '有効な値が入力されたとき' do
      before do
        visit signup_path
        fill_in '名前', with: 'testuser'
        fill_in 'メールアドレス', with: 'testuser@example.com'
        fill_in 'パスワード', with: 'password'
        fill_in 'パスワード', with: 'password'
        click_button '新規登録'
      end

      it '登録が成功しフラッシュメッセージが出る' do
        visit root_path
        expect(has_css?('.alert-success')).to be_falsy
      end
    end

    context '無効な値が入力されたとき' do
      subject { page }

      before do
        visit signup_path
        fill_in '名前', with: ''
        fill_in 'メールアドレス', with: ''
        fill_in 'パスワード', with: ''
        fill_in 'パスワード', with: ''
        click_button '新規登録'
      end

      it '登録が失敗する為エラーが出る' do
        is_expected.to have_selector('#error_explanation')
        is_expected.to have_selector('.alert-danger')
        expect(page).to have_content('フォームには4 個のエラーがあります')
        is_expected.to have_content('パスワードを入力してください')
      end

      it '今いるページのURLの検証' do
        is_expected.to have_current_path '/signup'
      end
    end
  end
end
