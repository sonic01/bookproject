require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryBot.create(:user) }

  describe User do
    it '有効なファクトリを持つこと' do
      expect(build(:user)).to be_valid
    end
  end

  # Shoulda Matchers を使用
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_length_of(:name).is_at_most(50) }
  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_length_of(:email).is_at_most(255) }
  it { is_expected.to validate_presence_of :password }
  it { is_expected.to validate_length_of(:password).is_at_least(6) }

  it '重複したメールアドレスなら無効な状態であること' do
    FactoryBot.create(:user, email: 'aaron@example.com')
    user = FactoryBot.build(:user, email: 'Aaron@example.com')
    user.valid?
    expect(user.errors[:email]).to include('はすでに存在します')
  end

  describe 'メールアドレスの有効性を検証' do
    it '無効なメールアドレスの場合' do
      invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.foo@bar_baz.com foo@bar+baz.com]
      invalid_addresses.each do |invalid_address|
        user.email = invalid_address
        expect(user).not_to be_valid
      end
    end

    it '有効なメールアドレスの場合' do
      valid_addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      valid_addresses.each do |valid_address|
        user.email = valid_address
        expect(user).to be_valid
      end
    end
  end

  describe 'パスワード確認が一致することを検証' do
    it 'パスワードが一致する場合' do
      user = FactoryBot.build(:user, password: 'password', password_confirmation: 'password')
      expect(user).to be_valid
    end

    it 'パスワードが一致しない場合' do
      user = FactoryBot.build(:user, password: 'password', password_confirmation: 'different')
      user.valid?
      expect(user.errors[:password_confirmation]).to include('とパスワードの入力が一致しません')
    end
  end

  describe 'パスワードの長さと空文字について検証' do
    describe 'パスワードの存在を検証する' do
      it 'パスワードが空白の場合は無効' do
        user = build(:user, password: ' ' * 6)
        expect(user).not_to be_valid
      end
    end

    it { is_expected.to validate_length_of(:password).is_at_least(6) }
  end

  describe 'コールバック(before_save)を行い、email属性を強制的に小文字に変換する' do
    describe '#email_downcase' do
      let!(:user) { create(:user, email: 'ORIGINAL@EXAMPLE.COM') }

      it 'makes email to low case' do
        expect(user.reload.email).to eq 'original@example.com'
      end
    end
  end

  # 2つのバグのテスト2
  describe 'authenticated?ダイジェストがnilのユーザーにはfalseを返す必要があります' do
    it 'ダイジェストが存在しない場合のauthenticated?のテスト' do
      user = User.new(name: 'Example User', email: 'user@example.com', password: 'foobar', password_confirmation: 'foobar')
      user.authenticated?('')
      expect(user).to be_truthy
    end
  end

  describe 'マイクロポイントの削除について' do
    it 'ユーザが削除されたら投稿も削除される' do
      user.microposts.create!(title: 'destroy title')
      expect { user.destroy }.to change { Micropost.count }.by(-1)
    end
  end
end
