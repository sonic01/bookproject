RSpec.describe Micropost, type: :model do
  let(:user) { create(:user) }
  let(:micropost) { user.microposts.build(title: 'Example title', content: 'example content', user_id: user.id) }

  describe 'Micropost' do
    it '有効なマイクロポイントを持つこと' do
      expect(micropost).to be_valid
    end
  end

  describe 'タイトルの有効性を検証' do
    it '有効なタイトルの場合' do
      micropost.update_attributes(title: 'success title', content: 'success content', picture: nil, user_id: user.id)
      expect(micropost).to be_valid
    end
    it '無効なタイトルの場合' do
      micropost.update_attributes(title: nil, content: 'Failure content', picture: nil, user_id: user.id)
      expect(micropost).to be_invalid
    end
  end

  describe 'user_idを検証' do
    it '無効なuser_idの場合' do
      micropost.user_id = nil
      expect(micropost).to be_invalid
    end
  end

  describe '文字数を検証' do
    it '投稿内容の文字数を検証' do
      micropost.content = 'a' * 140
      expect(micropost).to be_valid
      micropost.content = 'a' * 141
      expect(micropost).to be_invalid
    end
    it 'タイトルの文字数を検証' do
      micropost.title = 'a' * 100
      expect(micropost).to be_valid
      micropost.title = 'a' * 101
      expect(micropost).to be_invalid
    end
  end

  describe 'カラムの表示順を検証' do
    let!(:day_before_yesterday) { FactoryBot.create(:micropost, :day_before_yesterday) }
    let!(:now) { FactoryBot.create(:micropost, :now) }
    let!(:yesterday) { FactoryBot.create(:micropost, :yesterday) }

    it 'マイクロポイントが降順に表示される' do
      expect(Micropost.first).to eq now
    end
  end
end
