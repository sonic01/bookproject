require 'rails_helper'

RSpec.describe 'Remember me', type: :request do
  let(:user) { FactoryBot.create(:user) }

  # 2つのバグのテスト1
  describe '2番目のウィンドウでログアウトをクリックするユーザーをシミュレートする' do
    it 'ログイン中のみログアウトすること' do
      # spec/support/utilities.rbに定義
      sign_in_as(user)
      expect(response).to redirect_to user_path(user)

      # ログアウトする
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil

      # 2番目のウィンドウでログアウトする
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil
    end
  end

  describe 'remember_meチェックボックスのテスト' do
    it 'クッキーを保存してログイン' do
      log_in_as(user)
      expect(response.cookies['remember_token']).not_to eq nil
    end
    it 'クッキーを保存せずにログイン' do
      log_in_as(user)
      delete logout_path
      log_in_as(user, remember_me: '0')
      expect(response.cookies['remember_token']).to eq nil
    end
  end
end
