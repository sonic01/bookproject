require 'rails_helper'
RSpec.describe 'Microposts', type: :request do
  describe 'Micropostsコントローラの認可テスト' do
    describe '#create' do
      let(:micropost) { FactoryBot.attributes_for(:micropost) }
      let(:post_request) { post microposts_path, params: { micropost: micropost } }

      context 'ログインしていないとき' do
        it 'マイクロポイントの数が変動していない事' do
          expect { post_request }.to change(Micropost, :count).by(0)
        end

        it 'ログインページにリダイレクトする' do
          expect(post_request).to redirect_to login_url
        end
      end
    end

    describe '#destroy' do
      let!(:micropost) { FactoryBot.create(:micropost) }
      let(:delete_request) { delete micropost_path(micropost) }

      context 'ログインしていないとき' do
        it 'マイクロポイントの数が変動していない事' do
          expect { delete_request }.to change(Micropost, :count).by(0)
        end

        it 'ログインページにリダイレクトする' do
          expect(delete_request).to redirect_to login_url
        end
      end

      context '間違ったユーザによるマイクロポスト削除に対してテスト' do
        let(:user) { FactoryBot.create(:user) }

        before { log_in_as(user) }

        it 'マイクロポイントの数が変動していない事' do
          expect { delete_request }.to change(Micropost, :count).by(0)
        end

        it 'ログインページにリダイレクトする' do
          expect(delete_request).to redirect_to root_url
        end
      end
    end
  end
end
