class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page], per_page: 10)
      @user = current_user
      @tags = @user.microposts.tag_counts_on(:tags).order('count DESC')
    end
  end
end
