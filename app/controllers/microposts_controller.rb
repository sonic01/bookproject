class MicropostsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: :destroy
  before_action :set_locale

  def index
    @user = current_user
    @micropost = Micropost.where(user_id: @user.id)
    @tags = @user.microposts.tag_counts_on(:tags).order('count DESC')
    if params[:tag]
      @tag = params[:tag]
      @micropost = @user.microposts.tagged_with(params[:tag])
    end
  end

  def show
    @microposts = Micropost.find(params[:id])
    @tags = @micropost.tag_counts_on(:tags)
  end

  def new
    @micropost = Micropost.new
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.update_attributes(micropost_params)
      flash[:success] = '投稿を編集しました‼'
      redirect_to root_url
    else
      @feed_items = []
      render '/microposts/new'
    end
  end

  def edit
    @micropost = Micropost.find(params[:id])
  end

  def update
    @micropost = Micropost.find(params[:id])
    if @micropost.update_attributes(micropost_params)
      flash[:success] = '投稿を編集しました‼'
      redirect_to root_url
    else
      render '/microposts/edit'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = '投稿を削除しました。'
    redirect_to request.referrer || root_url
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :title, :picture, :number, :tag_list)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end

  def set_locale
    I18n.locale = :ja
  end
end
