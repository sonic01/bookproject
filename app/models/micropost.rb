class Micropost < ApplicationRecord
  belongs_to :user
  acts_as_taggable
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 100 }
  validates :content, length: { maximum: 140 }
  validate  :picture_size
  validates :number, numericality: { only_integer: true, allow_nil: true }

  private

  # アップロードされた画像のサイズをバリデーションする
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, '5MB未満である必要があります')
    end
  end
end
