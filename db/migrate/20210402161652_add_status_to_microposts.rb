class AddStatusToMicroposts < ActiveRecord::Migration[5.2]
  def change
    add_column :microposts, :title, :string
    add_column :microposts, :number, :integer
  end
end
