# portfolio_app[Bookreco]
It is an auxiliary application that remembers the contents of the bookshelf, not the forgetful self.
忘れっぽい自分ではなく、本棚の中身を記憶する補助的なアプリケーションです。

## Description
It is a work that outputs learning and deepens understanding. Created for book management.
Created to manage bookshelves even when not at home.
By managing the bookshelf, you can not only prevent duplicate purchases, but also manage lending and borrowing, so it is a safe auxiliary application that you can remember.
自分自身の理解を深めるための学習をアウトプットした作品です。家にいなくても本棚を管理するために作成しました。
本棚を管理することで、重複購入を防ぐだけでなく、貸し借りも管理できるので、覚えておくことができる補助アプリです。

## Usage
### Homebrew installation.　homebrew のインストール
Please install [homebrew] (https://brew.sh/index_ja.html) to install rubies and other packages.
rubyのインストールや、その他のパッケージのインストールのため、[homebrew](https://brew.sh/index_ja.html)をインストールします。

Open a terminal and enter the following command.
ターミナルを開き、下記コマンドを入力します。

```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Install Docker　Docker のインストール
```bash
brew update
brew install caskroom/cask/brew-cask
brew cask install docker
```
### Start Docker　Docker の起動
```bash
open /Applications/Docker.app
```

### docker-compose up
Move to the development directory and execute the following command in the terminal.開発ディレクトリに移動し、ターミナルで下記コマンドを実行します
```bash
docker-compose up --build
```

## Requirements
- rails 5.2.1
- ruby 2.5.1
- BUNDLED 1.17.3

## Licence
Copyright (c) 2020 Yuriko Igusa

## Author
* Yuriko Igusa　作成者
* E-mail neroexplosionalbatrossblindfold@yahoo.co.jp
